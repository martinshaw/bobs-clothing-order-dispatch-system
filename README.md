# Bob's Clothing - Order Dispatch System

Programming challenge task implemented as part of a job application

## Brief

This challenge is designed to demonstrate your PHP (5.6+ - Ideally 7+) OO design and implementation skills in a real-world environment.
Time allowed: 90 minutes

### Scenario

We have a client, Bobs Clothing, who sell fashion clothing online. Bob has asked us to build an order dispatch system, for sending out customer
orders to numerous couriers. At the start of a normal working day, a new batch will be started, and it will be closed at the end of the day, when
no more parcels are going to be shipped. This is called the dispatch period.

Each parcel sent out with a courier is called a consignment. Each consignment will be given a unique number - each courier will supply an
algorithm for generating their own format of consignment numbers.

At the end of each dispatch period, a list of all the consignment numbers needs to be sent to each individual courier. The method of data
transport varies from courier to courier (e.g. Royal Mail use email, ANC use anonymous FTP).

### What you should produce

Build a class structure to facilitate the implementation of the scenario set out above. Assume that your class library will be given to another
developer at a later date to build the interface for the client.

The client interface will have three primary functions;
1.  Start new batch
2.  Add consignment
3.  End current batch

### What we are evaluating

We’ll be reviewing your OO analysis and design skills based on the object structure you choose and to some extent your
understanding and ability with PHP(5.6+) based on the code you produce. Use class/method commenting (ideally phpDoc format) to
explain your design decisions. Code re use, extensibility and maintenance are all important.

Make as much progress as you feel you can within the time allowed; you are not required to produce a final product.
Do not worry about persistent storage/database abstraction layers/html/visuals - focus on the business logic and class structures.

NOTE: We would prefer that you email the code for review in a zip file, rather than post on a public repo like GitHub


## Implementation Details 

Entity types mentioned in the Brief above:

*  Courier
*  DispatchPeriod
*  Parcel
*  Consignment