<?php
namespace BobsClothing\OrderDispatchSystem;

use BobsClothing\OrderDispatchSystem\Parcel;
use BobsClothing\OrderDispatchSystem\Couriers\Courier;

/**
 * Class for Consignment which describes a schedulable attempt to send a Parcel to a Courier
 */
class Consignment implements \JsonSerializable
{
    /**
     * Consignment identifier for internal use. This would be used for association in a database or persistent storage.
     *
     * @var int
     */
    protected $id = 1;

    /**
     * Courier-provided identifier for this Consignment
     *
     * @var string
     */
    protected $courierId;
    
    /**
     * Parcel class instance representing the item to be shipped in this Consignment
     *
     * @var Parcel
     */
    protected $parcel;
    
    /**
     * Courier class instance representing the courier which will receive the Parcel in this Consignment
     *
     * @var Courier
     */
    protected $courier;

    /**
     * User-friendly name for Consignment to be displayed in user interfaces
     * @var string
     */
    protected $name = 'Consignment';

    /**
     * Date within the associated Dispatch Period when Consignment was created
     * @var DateTime
     */
    protected $dateCreated;
    
    /**
     * Consignment Constructor
     *
     * @param  Parcel $parcel
     * @param  Courier $courier
     * @return self
     */
    public function __construct(Parcel $parcel, Courier $courier, ?\DateTime $dateCreated = null)
    {
        $this->name = "Consignment #{$this->id}";
        $this->parcel = $parcel;
        $this->courier = $courier;
        $this->dateCreated = empty($dateCreated) ? (new \DateTime) : $dateCreated;

        $this->courierId = $courier->generateConsignmentIdentifier($this);
    }
    
    /**
     * Send this Consignment to the associated Courier
     *
     * @return bool
     */
    public function sendToCourier() : bool
    {
        return $this->courier->sendConsignmentToCourier($this);
    }

    /**
     * Getter for this Consignment's internal identifier
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Getter for this Consignment's Courier identifier
     *
     * @return string
     */
    public function getCourierId() : string
    {
        return $this->courierId;
    }

    /**
     * Getter for this Consignment's Parcel instance
     *
     * @return Parcel
     */
    public function getParcel() : Parcel
    {
        return $this->parcel;
    }

    /**
     * Getter for this Consignment's Courier instance
     *
     * @return Courier
     */
    public function getCourier() : Courier
    {
        return $this->courier;
    }
    
    /**
     * Getter for this Consignment's name
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    
    /**
     * Getter for date when Consignment was created
     *
     * @return \DateTime
     */
    public function getDateCreated() : \DateTime
    {
        return $this->dateCreated;
    }
    
    /**
     * Serialised representation of Consignment object instance
     *
     * @return array
     */
    public function jsonSerialize() : array
    {
        return [
            'id' => $this->getId(),
            'courierId' => $this->getCourierId(),
            'parcel' => $this->getParcel(),
            'courier' => $this->getCourier(),
            'name' => $this->getName(),
            'dateCreated' => $this->getDateCreated(),
        ];
    }
}
