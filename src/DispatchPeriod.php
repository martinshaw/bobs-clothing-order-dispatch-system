<?php
namespace BobsClothing\OrderDispatchSystem;

use BobsClothing\OrderDispatchSystem\Parcel;
use BobsClothing\OrderDispatchSystem\Couriers\Courier;
use BobsClothing\OrderDispatchSystem\Exceptions\DispatchPeriodAddConsignmentException;

/**
 * Class for Dispatch Period which describes a batch of Consignments to be shipped to their Couriers within a daily period
 */
class DispatchPeriod implements \JsonSerializable
{    
    /**
     * Batch of Consignments to be shipped to their Couriers within this Dispatch Period
     *
     * @var array
     */
    protected $batch = [];
    
    /**
     * Start of schedulable date associated with this Dispatch Period
     *
     * @var DateTime
     */
    protected $periodStart;
    
    /**
     * End of schedulable date associated with this Dispatch Period
     *
     * @var \DateTime
     */
    protected $periodEnd;
    
    /**
     * Dispatch Period Constructor
     *
     * @param array $batch Array of Consignments
     * @param string $date The date which this Dispatch Period represents. In 'DD/MM/YYYY' format.
     * @return self
     * @throws DispatchPeriodAddConsignmentException
     */
    public function __construct(array $batch = [], ?string $date = '')
    {
        $this->setPeriodFromDateTime(empty($date) ? (new \DateTime) : (new \DateTime($date)));
        foreach ($batch as $consignment) {
            $this->addConsignment($consignment);
        }
    }
    
    /**
     * Populates dispatch period start and end date time values based on provided day date
     *
     * @param \DateTime $date The date which this Dispatch Period represents.
     * @return self
     */
    public function setPeriodFromDateTime(\DateTime $date) : self
    {
        $this->periodStart = new \DateTime($date->format('m/d/Y') . ' 00:00:00');
        $this->periodEnd = (new \DateTime($date->format('m/d/Y') . ' 00:00:00'))->add(new \DateInterval('P1D'));

        return $this;
    }
    
    /**
     * Returns whether the Dispatch Period's batch is empty
     *
     * @return bool
     */
    public function batchIsEmpty() : bool
    {
        return count($this->batch) < 1;
    }

    /**
     * Returns whether the provided date occurs within this Dispatch Period
     *
     * @param  mixed $date
     * @return bool
     */
    public function dateIsWithinPeriod(\DateTime $date) : bool
    {
        return ($date >= $this->periodStart and $date < $this->periodEnd);
    }
    
    /**
     * Add Consignment to this Dispatch Period's batch
     *
     * @param  Consignment $consignment
     * @return self
     * @throws DispatchPeriodAddConsignmentException
     */
    public function addConsignment(Consignment $consignment) : self
    {
        if ($this->dateIsWithinPeriod($consignment->getDateCreated()) === false) {
            throw new DispatchPeriodAddConsignmentException(
                "Consignment #{$consignment->getId()} was not created during this Dispatch Period and therefore cannot be added"
            );
        }
        $this->batch[] = $consignment;
        
        return $this;
    }
    
    /**
     * Send each Consignment in batch to their associated Couriers
     *
     * @return bool
     */
    public function sendBatchToCouriers() : bool
    {
        foreach ($this->getBatch() as $consignment) {
            $result = $consignment->sendToCourier();
            if ($result !== true) {
                return $result;
            }
        }
        return true;
    }

    /**
     * Getter for this Dispatch Period's batch of Consignments
     *
     * @return array
     */
    public function getBatch() : array
    {
        return $this->batch;
    }

    /**
     * Getter for datetime value at the start of schedulable date associated with this Dispatch Period
     *
     * @return \DateTime
     */
    public function getPeriodStart() : \DateTime
    {
        return $this->periodStart;
    }

    /**
     * Getter for datetime value at the end of schedulable date associated with this Dispatch Period
     *
     * @return \DateTime
     */
    public function getPeriodEnd() : \DateTime
    {
        return $this->periodEnd;
    }
    
    /**
     * Serialised representation of Dispatch Period object instance
     *
     * @return array
     */
    public function jsonSerialize() : array
    {
        return [
            'batch' => $this->getBatch(),
            'periodStart' => $this->getPeriodStart(),
            'periodEnd' => $this->getPeriodEnd(),
        ];
    }
}
