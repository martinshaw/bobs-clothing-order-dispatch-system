<?php
namespace BobsClothing\OrderDispatchSystem;

/**
 * Class for Parcel which describes an item which may be shipped to a Courier as part of a Consignment
 */
class Parcel implements \JsonSerializable
{
    /**
     * Parcel identifier for internal use. This would be used for association in a database or persistent storage.
     *
     * @var int
     */
    protected $id = 1;

    /**
     * User-friendly name for Parcel to be displayed in user interfaces
     * @var string
     */
    protected $name = 'Parcel';
    
    /**
     * Getter for this Parcel's identifier
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    
    /**
     * Getter for this Parcel's name
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    
    /**
     * Serialised representation of Parcel object instance
     *
     * @return array
     */
    public function jsonSerialize() : array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }
}
