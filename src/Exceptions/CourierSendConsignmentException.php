<?php
namespace BobsClothing\OrderDispatchSystem\Exceptions;

/**
 * Exception which may be thrown by the Courier-specific implementaion of method which sends Consignment data to Courier
 */
class CourierSendConsignmentException extends \Exception
{    
    /**
     * CourierSendConsignmentException constructor
     *
     * @param string|null $messageSuffix
     * @return void
     */
    public function __construct(?string $messageSuffix)
    {
        $message = 'An error occurred when attempting to send Consignment data to Courier';
        if (empty($messageSuffix) === false) {
            $message .= ': ' . $messageSuffix;
        }

        parent::__construct($message);
    }
}
