<?php
namespace BobsClothing\OrderDispatchSystem\Exceptions;

/**
 * Exception which may be thrown by the Dispatch Period when attempting to add a Consignment which was not created during the defined period date
 */
class DispatchPeriodAddConsignmentException extends \Exception
{    
    /**
     * DispatchPeriodAddConsignmentException constructor
     *
     * @param string|null $messageSuffix
     * @return void
     */
    public function __construct(?string $messageSuffix)
    {
        $message = 'An error occurred when attempting to add Consignment to Dispatch Period';
        if (empty($messageSuffix) === false) {
            $message .= ': ' . $messageSuffix;
        }

        parent::__construct($message);
    }
}
