<?php
namespace BobsClothing\OrderDispatchSystem\Couriers;

use BobsClothing\OrderDispatchSystem\Consignment;
use BobsClothing\OrderDispatchSystem\Exceptions\CourierSendConsignmentException;
use BobsClothing\OrderDispatchSystem\Couriers\Traits\FTPTransportTrait;

/**
 * Sub-class for ANC Courier which describes the generation of Consignment identifiers and the
 * method of data transport to be performed at the end of a dispatch period.
 */
class ANCCourier extends Courier
{
    use FTPTransportTrait;

    /**
     * User-friendly name for Courier to be displayed in user interfaces
     * @var string
     */
    protected $name = 'ANC';

    /**
     * Generates Consignment identifier based on randomly generated hexidecimal number
     * 
     * @param Consignment $consignment
     * @return string Consignment identifier 
     */
    public function generateConsignmentIdentifier(Consignment $consignment) : string
    {
        $date = date('Y-m-d\TH:i:s');
        return 'ANC_' . bin2hex(random_bytes(16)) . '_' . $date;
    }

    /**
     * Send Consignment information to the Courier
     * 
     * @param Consignment $consignment
     * @return bool Returns true when Consignment was successfully sent to Courier
     * @throws CourierSendConsignmentException
     */
    public function sendConsignmentToCourier(Consignment $consignment) : bool
    {
        $result = $this->sendContentsToFtpFile(
            'http://localhost:21',
            'username',
            'password',
            json_encode($consignment),
            '/uploads/bobs_clothing/consignments/' . date('Y-m-d\TH:i:s') . '/' . $consignment->getCourierId()
        );
        if ($result === false) {
            throw new CourierSendConsignmentException(error_get_last()['message']);
        }
        
        return true;
    }
}
