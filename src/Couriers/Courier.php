<?php
namespace BobsClothing\OrderDispatchSystem\Couriers;

use BobsClothing\OrderDispatchSystem\Consignment;
use BobsClothing\OrderDispatchSystem\Exceptions\CourierSendConsignmentException;

/**
 * Generic abstract class for Courier which describes the generation of Consignment identifiers and the
 * method of data transport to be performed at the end of a dispatch period.
 */
abstract class Courier
{
    /**
     * User-friendly name for Courier to be displayed in user interfaces
     * @var string
     */
    protected $name = 'Courier';

    /**
     * Generates Consignment identifier based on Courier-specific implementation
     * 
     * @param Consignment $consignment Consignment is made available to implementation method when generating identifier
     * @return string Consignment identifier 
     */
    abstract public function generateConsignmentIdentifier(Consignment $consignment) : string;

    /**
     * Send Consignment information to the Courier based on Courier-specific implementation
     * 
     * @param Consignment $consignment Consignment is made available to implementation method when sending data to Courier
     * @return bool Returns true when Consignment was successfully sent to Courier
     * @throws CourierSendConsignmentException
     */
    abstract public function sendConsignmentToCourier(Consignment $consignment) : bool;

    /**
     * Returns user-friendly name of Courier
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
