<?php
namespace BobsClothing\OrderDispatchSystem\Couriers;

use BobsClothing\OrderDispatchSystem\Consignment;
use BobsClothing\OrderDispatchSystem\Exceptions\CourierSendConsignmentException;

/**
 * Sub-class for Royal Mail Courier which describes the generation of Consignment identifiers and the
 * method of data transport to be performed at the end of a dispatch period.
 */
class RoyalMailCourier extends Courier
{
    /**
     * User-friendly name for Courier to be displayed in user interfaces
     * @var string
     */
    protected $name = 'Royal Mail';

    /**
     * 'From' e-mail address used when sending Consignment data to Courier
     * @var string
     */
    protected $emailFrom = 'from.royalmail.dummy@martinshaw.co';

    /**
     * 'To' e-mail address used when sending Consignment data to Courier
     * @var string
     */
    protected $emailTo = 'to.royalmail.dummy@martinshaw.co';

    /**
     * Generates Consignment identifier based on randomly generated hexidecimal number
     * 
     * @param Consignment $consignment
     * @return string Consignment identifier 
     */
    public function generateConsignmentIdentifier(Consignment $consignment) : string
    {
        return bin2hex(random_bytes(16));
    }

    /**
     * Send Consignment information to the Courier
     * 
     * @param Consignment $consignment
     * @return bool Returns true when Consignment was successfully sent to Courier
     * @throws CourierSendConsignmentException
     */
    public function sendConsignmentToCourier(Consignment $consignment) : bool
    {
        $subject = 'Bob\'s Clothing Order Dispatch System - Consignment ' . $consignment->getCourierId();
        $message = 'This is a dummy test e-mail which might contain information about the Consignment.';
        $headers = 'From:' . $this->emailFrom;

        $result = mail($this->emailTo, $subject, $message, $headers);
        if ($result === false) {
            throw new CourierSendConsignmentException(error_get_last()['message']);
        }

        return true;
    }
}
