<?php
namespace BobsClothing\OrderDispatchSystem\Couriers\Traits;

use BobsClothing\OrderDispatchSystem\Exceptions\CourierSendConsignmentException;

/**
 * Trait which shares simplified FTP transport functionality with Couriers such as ANCCourier
 */
trait FTPTransportTrait
{
    protected $ftpConnectionId;

    /**
     * Create a new temp. file with provided contents, send file via FTP to destination path, then remove temporary file
     *
     * @param  string $hostname
     * @param  string $username
     * @param  string $password
     * @param  string $content
     * @param  string $destinationPath
     * @return bool
     */
    protected function sendContentsToFtpFile(string $hostname, string $username, string $password, string $content, string $destinationPath) : bool
    {
        $this->connectToFtp($hostname, $username, $password);
        
        $tempFile=fopen('php://temp', 'r+');
        fwrite($tempFile, $content);
        rewind($tempFile);

        $result = $this->uploadFileToFtp($tempFile, $destinationPath);

        ftp_close($this->ftpConnectionId);
        fclose($tempFile);

        return true;
    }
    
    /**
     * Initiates connection to FTP server
     *
     * @param  mixed $hostname
     * @param  mixed $username
     * @param  mixed $password
     * @return bool
     */
    public function connectToFtp(string $hostname, string $username, string $password) : bool
    {
        $this->ftpConnectionId = ftp_connect($hostname);
        $loginResult = ftp_login($this->ftpConnectionId, $ftpUser, $ftpPassword);
        
        if ((!$this->ftpConnectionId) || (!$loginResult)) {
            throw new CourierSendConsignmentException('An error occurred when attempting to connect to FTP server with provided credentials');
        }
        return true;
    }
    
    /**
     * Uploads file to a destination path on the FTP server
     *
     * @param string $originFile
     * @param string $destinationPath
     * @return bool
     */
    public function uploadFileToFtp(string $originFile, string $destinationPath) : bool
    {
        if (ftp_fput($this->ftpConnectionId, $fileTo, $fileFrom, FTP_ASCII) === false) {
            throw new CourierSendConsignmentException('An error occurred when attempting to upload file to FTP server');
        }
        return true;
    }
}
