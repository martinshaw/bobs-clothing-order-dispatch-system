<?php

require __DIR__ . '/../vendor/autoload.php';

$parcels = [
    new BobsClothing\OrderDispatchSystem\Parcel,
    new BobsClothing\OrderDispatchSystem\Parcel,
];

$courier = new BobsClothing\OrderDispatchSystem\Couriers\RoyalMailCourier;

$consignments = [
    new BobsClothing\OrderDispatchSystem\Consignment($parcels[0], $courier),
    new BobsClothing\OrderDispatchSystem\Consignment($parcels[1], $courier),
];

$dispatchPeriod = new BobsClothing\OrderDispatchSystem\DispatchPeriod($consignments);

echo '<pre>' . json_encode($dispatchPeriod, JSON_PRETTY_PRINT) . '</pre>';